// Examples of the JavaScript string functions

/* 1. charAt(x):
        Returns the character at the specified position (in Number).
*/        

var myString = 'PavithraRamamoorthy';
console.log("The lenngth of the myString is : " + myString.length)
console.log("The 8th and 0th character of myString is : " + myString.charAt(8),myString.charAt(0));
//Ans: The 8th and 1st character of myString is : R a


/* 2. concat(x1, x2,…) 
        Joins specified string literal values (specify multiple strings separated by comma) and
         returns a new string.
*/
var message = "pavithra";
var final = message.concat(" is "," Graduated at MCA");
console.log("concatnation of final message is : " + final);



/* 3. fromCharCode(c1, c2,…)
        Returns a string created by using the specified sequence of Unicode values. 
        For example: String.fromCharCode().
*/
console.log(String.fromCharCode(97,98,99,120,121,122))
//Ans: abcxyz

console.log(String.fromCharCode(72,69,76,76,79))
//Ans: HELLO


/* 4. indexOf(substr, [start]): 
        Returns the index of first occurrence of specified String starting from specified number index. 
        Returns -1 if not found.
*/
var sentence = "Hi, my name is PavithraRamamoorthy!"
if (sentence.indexOf("Ram")!=-1)
console.log("Ram is in there!")


/* 5. lastIndexOf(substr, [start]): 
        Returns the last occurrence index of specified SearchString, starting from specified position. 
        Returns -1 if not found.

*/

var myString = 'javascript';
console.log("The last index of r is : " + myString.lastIndexOf('r'));
//Ans: 


/* 6. replace(substr, replacetext): 
        Search specified string value and replace with specified replace Value string and return new string. 
*/

var myString = 'python Coders';
console.log("Replace the mystring python coders to : " + myString.replace(/python/i, "JavaScript"));
//Ans: JavaScript Coders

/* 7. slice(start, [end]):
        Extracts a section of a string based on specified starting and ending index and returns a new string.
*/

var str = "excellent"
console.log("The slice of Str(0,4) is : " + str.slice(0,4)) //returns "exce"
console.log("The slice of Str(2,4) is : " + str.slice(2,4)) //returns "ce"

/* 8. split():
        Splits a String into an array of strings by separating the string into 
        substrings based on specified separator.
*/

var message = "Welcome to JavaScript"
var word = message.split("l")
console.log("The splited string is : " + word);


/* 9. substr(start, [length]): 
        Returns the characters in a string from specified starting position through the 
        specified number of characters (length).
*/

var str = "excellent"
console.log("The substring of str is : " + str.substring(0,4)); //returns "exce"
console.log("The substring of str is : " + str.substring(2,4)) //returns "ce"


/* 10. substring(from, [to]):
        Returns the characters in a string between start and end indexes.
*/

var myString = 'javascript coders';
myString = myString.substring(0,10);
console.log("Substring of mystring from 0 to 10 is : " + myString)

/* 11. toLowerCase(): 
        Returns the string with all of its characters converted to lowercase.
*/

var myString = 'JAVASCRIPT CODERS';
myString = myString.toLowerCase();
console.log("The LowerCase of myString is : " + myString);

/* 12. toUpperCase(): 
        Returns the string with all of its characters converted to uppercase.
*/

var myString = 'javascript coders';
myString = myString.toUpperCase();
console.log("The UpperCase of myString is : " + myString);

/* 13.search(str): 
        Tests for a match in a string. It returns the index of the match, or -1 if not found.
*/
 
var myStr = 'Welcome to bangaloore!';
var myString = myStr.search('bangaloore');
console.log(myString);

/*14. includes():
        It is used to check whether a string contains the specified string or characters.
*/

var mystring = "Hello, welcome to India";
var str1 = mystring.includes("India");
console.log(str1)
//Ans: True

/* 15. endsWith():
        This function checks whether a string ends with specified string or characters.
*/        

var mystr = "List of javascript functions";
var str1 = mystr.endsWith("functions");
console.log(str1);
//Ans: True

/* 16. repeat():
        This returns a new string with a specified number of copies of an existing string.
*/

var string = "Welcome to India";
console.log(string.repeat(2));
//Ans: Welcome to India Welcome to India

/* 17. valueOf(): 
        It is used to return the primitive value of a String object.
*/

var mystr = "Hello World!";
var result = mystr.valueOf();
console.log(result);
//Ans: Hello World!

/* 18. trim():
        This function removes whitespace from both ends of a string.
*/
var str = "     Hello India!     ";
console.log(str.trim());
console.log(str.trimLeft());
console.log(str.trimRight());

/* 19.toString():
        It provides a string representing the particular object.
*/
var arr= [0,1,2,3,4,5,6,7,8,9]

var arr1 = arr.toString();
console.log(arr1);
//Ans:"0,1,2,3,4,5,6,7,8,9"

console.log(arr1.split(" "));
//Ans: ["0,1,2,3,4,5,6,7,8,9"]

console.log(arr1.split());
//Ans: ["0,1,2,3,4,5,6,7,8,9"]

console.log(arr1.split(""));
//Ans: (19) ["0", ",", "1", ",", "2", ",", "3", ",", "4", ",", "5", ",", "6", ",", "7", ",", "8", ",", "9"]

/* 20.length():
        Returns the length of the string.
*/

var name = "PavithraRamamoorthy";
console.log("The name of the length is : " + name);
//Ans: The name of the length is : PavithraRamamoorthy