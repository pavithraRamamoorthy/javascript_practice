function addThreeNumbers(numOne, numTwo, numThree){ 
        /* 
            Goal: Add Three numbers using addThree ffunction & return a single value or returns a JSON as response.
            Example response: { "message": "Invalid Inputs. This function accepts only integers." }
            Example valid addition (float or integer based on the input): 40.50 or 40 
        */
        var resultOfAddition    = 0;
            // 1. validate inputs
                    var validationResponse = validateInputs(numOne, numTwo, numThree);
                    if(!validationResponse){
                        return { "message": "Invalid Inputs. This function accepts only integers." }
                    }        
            // 2. implement business logic
                    resultOfAddition    = parseFloat(numOne) + parseFloat(numTwo) + parseFloat(numThree); 
            // 3. respond (return a value)
                    return resultOfAddition;        
        
    }
    
    function validateInputs(numOne, numTwo, numThree){
            
        if(typeof numOne == "string" || typeof numTwo == "string" || typeof numThree == "string"){
                if(parseFloat(numOne) == NaN || parseFloat(numTwo) == NaN || parseFloat(numThree) == NaN) {
                    return false;
                }
            }
            return true;
}

console.log(addThreeNumbers(2,3,5))