/*
Practice 1. Using Chrome's JavaScript console, write a function that takes a rank and a suit as input, 
and returns a string representation of a card. Use it to print out a few cards with various suits and ranks. 
*/

function Card(rank, suit) {

        this.rank = rank;
        this.suit = suit;
      
        this.toString  = cardToString;
      }

function cardToString() {

var rank, suit;
      
switch (this.rank) {
case "A" :
        rank = "Ace";
        break;
case "2" :
        rank = "Two";
        break;
case "3" :
        rank = "Three";
        break;
case "4" :
        rank = "Four";
        break;
case "5" :
        rank = "Five";
        break;
case "6" :
        rank = "Six";
        break;
case "7" :
        rank = "Seven";
        break;
case "8" :
        rank = "Eight";
        break;
case "9" :
        rank = "Nine";
        break;
case "10" :
        rank = "Ten";
        break;
case "J" :
        rank = "Jack"
        break;
case "Q" :
        rank = "Queen"
        break;
case "K" :
        rank = "King"
        break;
default :
        rank = null;
        break;
}     

switch (this.suit) {
case "C" :
        suit = "Clubs";
        break;
case "D" :
        suit = "Diamonds"
        break;
case "H" :
        suit = "Hearts"
        break;
case "S" :
        suit = "Spades"
        break;
default :
        suit = null;
        break;
}

// 1. validate inputs

if (rank == null || suit == null)
{
        return "";
}        
// 2. implement business logic
// 3. respond (return a value)      
return rank + " of " + suit;
}

var myCard = new Card("A" , "S");
console.log(myCard.toString());
