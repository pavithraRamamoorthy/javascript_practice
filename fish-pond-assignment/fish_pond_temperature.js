/* pseudocode:
Goal: A temperature controller for a pond.

1. The input is Initial temperature = 30 C.
2. Used new Date() method and get the current time.
3. Used setInterval() and clearInterval() methods for repeat the process based on time and temperature condition.
4. When the temperature reaches 30 C, then it should be gradually increased by 0.02 C for each 5 minutes.(using if statements)
5. When the temperature reaches 40 C, then it should be gradually reduced by 0.01 C for each 2 minute.(using if statement)
6. Check the time and temperature conditions for every time when the temperature increase or decrease
7. during night time, the increasedTemperature is reduced by 50%.
*/


//Initial temperature is 30 C 
var initialTemperature = 30;

var date = new Date()
var time = date.getHours()

function setFishPondTemperature()
{
        var increaseCelcious =  setInterval(() => {
                if(time >= 4 && time <= 20) {
                //When the temperature reaches 30 C, then it should be gradually increased by 0.02 C for each 5 minutes.
                        if(initialTemperature >= 30)
                        {
                        initialTemperature += 0.02;
                        }
                } 
                console.log("The increasedCelcious Fish pond Temperature is : " + initialTemperature + "C");
                if(initialTemperature >= 40) {
                        clearInterval(increaseCelcious);
                }     
        },1000*300);  

        var decreaseCelcious = setInterval(() => {
                if(time >= 4 && time <= 20) {
                //When the temperature reaches 40 C, then it should be gradually reduced by 0.01 C for each 2 minutes.        
                        if(initialTemperature >= 40)
                        {
                        initialTemperature -= 0.01;
                        }
                } 
                console.log("The decreasedCelcious Fish pond Temperature is : " + initialTemperature + "C");
                if(initialTemperature <= 30) {
                        clearInterval(decreaseCelcious);
                }
        },1000*60);   
        
        if(time >= 21 || time <= 3)
        {       
                //during night time, the above behaviour is reduced by 50%.
                var nightTimeTemperature = initialTemperature/2;
                console.log("during night time, the increasedTemperature is reduced by 50% is : ", + nightTimeTemperature)
        }


}        

setFishPondTemperature();