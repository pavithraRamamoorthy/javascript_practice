/*4. Can you think of a way to determine which card appears the most frequently? Obviously, you can repeat 
the process above for all 52 combinations, but is there an automated way you can do it? By the end of all of 
the subsequent exercises, you should be able to do this using a single function. 
*/

console.log("The total cards is : " + cards.length)


var emptyArray = cards.filter(suits => suits.suit).map(element=>element.suit);
console.log("The total suit values in an array is : " +  emptyArray.length)

var occurances ={};
var currentVal =0;
var maxValue = emptyArray[0];

function mostFrequentSuitValue(emptyArray)
{
        for(let i=0; i<emptyArray.length; i++)
        {
                currentVal = emptyArray[i];
                if(occurances[currentVal]==undefined)
                {
                        occurances[currentVal]= 1;
                }
                else
                {
                        occurances[currentVal]++;
                }
                //console.log(occurances)

                if(occurances[currentVal] > occurances[maxValue])
                {
                maxValue = currentVal;
                }
        }
        return maxValue;
}
var mostFrequentlySuit = mostFrequentSuitValue(emptyArray);
console.log(occurances)
console.log("The card appears the most frequently item is : " + mostFrequentlySuit +" ( " +occurances[maxValue] + " times ) ");




var unique = [... new Set(emptyArray)]
console.log(unique)
console.log("The unique values are : (" + unique.length + ")" + unique);

/*Ans: The total cards is : 5000
The total suit values in an array is : 5000
{ clubs: 1238, diamonds: 1252, hearts: 1257, spades: 1253 }
The card appears the most frequently item is : hearts ( 1257 times ) 
[ 'clubs', 'diamonds', 'hearts', 'spades' ]
The unique values are : (4)clubs,diamonds,hearts,spades
*/