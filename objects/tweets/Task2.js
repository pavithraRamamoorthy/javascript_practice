//2. How many of the tweets contains URLs in them? (You can just look for "http:" as a substring). 

var result = tweets.filter(tweetsUrl =>{
    if(tweetsUrl.user.url!=null){
        return  tweetsUrl.user.url.indexOf("http:")!=-1
    }
}).map(element => element.user.url);
console.log(" tweets contains URLs in  : " + result.length);

//Ans: tweets contains URLs in  : 123