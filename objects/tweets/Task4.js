//4. What is the screen name of the user with the most followers? 

var count = tweets.filter(function(element){
        return element.user.followers_count;
    }).map(element => element.user.followers_count);
     
    console.log("The count length is : " + count.length); 
    
    
    var max = count.reduce(function(a, b) {
     return Math.max(a, b);
    });
    console.log("The maximum followers_count is : " + max);
    
    
    var screen_name = tweets.filter(screen=>{
    
     if(screen.user.followers_count ==max)
     {
       return screen.user.screen_name;
     }
    }).map(element=>element.user.screen_name);
    
    console.log("The screen_name are : " + screen_name)

/*Ans: The count length is : 496
The maximum followers_count is : 207124
The screen_name are : HannahSimone
*/