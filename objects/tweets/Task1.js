/* 1. Create an array that only contains only the tweet texts that contain the word "awesome" 
(upper or lower case). How many tweets are in the array? 
*/

console.log("The total tweets is : " + tweets.length)

var text = tweets.filter(function(element){
       return element.text.toLowerCase().indexOf("awesome") != -1;
}).length
    
console.log("The awesome text tweets are in the array is " + text);    

/*Ans: The total tweets is : 500
The awesome text tweets are in the array is 3
*/