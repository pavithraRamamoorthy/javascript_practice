//A class without a name is called anonymous, or unnamed classes

var Rectangle = class {

        constructor(height, width) 
        {
                this.height = height;
                this.width = width;
        }
        
        area() {
                return this.height * this.width;
        }
};
      
console.log(new Rectangle(5, 8).area());
//Ans: 40

