//1. Create a class called 'fan', assume your own data members, methods atleast 6 each and two private data members.

class Fan
{
        constructor()
        {
                var _myFans= 7; //private data  member
                var brand = "usha"
                this.wings = 3;
                this.color = "white";
                this.size = "10cm";
                this.fanSpeed = 80;
                this.electricMotor = "induction";
                this.fanCost = 7000;
        }

        calclateEstimateFanCost()
        {
          return "The Fan cost is : " + this.fanCost;
        }
}
var myFan = new Fan();
console.log("When we try to access the private member outside the class : " + myFan.brand);
var _fanCost = myFan.calclateEstimateFanCost();// calls calculateEstimateFanCost() from 'Fan'
console.log(_fanCost);

//2. Create a child class 'tableFan' from above 'fan'

class tableFan extends Fan
{
/*
Parent (Super class): 'Fan'
Child: 'tableFan'
*/        
        constructor()
        {
                super();
                this.color = "gray";
        }

        // Overrides calculateEstimateFanCost() from 'Fan'
        // Method overriding
        calclateEstimateFanCost()
        {
          return "The override Fan cost is : " + 7 * this.fanCost;
        }

        getFanColor()
        {
                return "The fan color is : " + this.color;
        }
}
var myTableFan = new tableFan();
var _tableFan = myTableFan.getFanColor();
console.log(_tableFan); //Ans: The fan color is : gray
console.log(myTableFan.calclateEstimateFanCost())
//Ans:The override Fan cost is : 49000

//3. Create a child class 'ceilingFan' from 'fan'
class ceilingFan extends Fan
{
/*
Parent (Super class): 'Fan'
Child: 'ceillingFan'
*/
        getFanMotorBrand()
        {
                return "The fan electricMotor Brand is : " + this.electricMotor;
        }
}
var myCeillingFan = new ceilingFan();
var _myceillingFan = myCeillingFan.getFanMotorBrand();
console.log(_myceillingFan);
//Ans: The fan electricMotor Brand is : induction


//4. Try to access private members from 'fan' in 'ceilingFan'. Is this allowed?
class ceilingFans extends Fan
{
        getFanBrand()
        {
                return "The fan Brand is : " + myFan.brand;
        }
}
var myCeillingFan = new ceilingFans();
var _myceillingFan = myCeillingFan.getFanBrand();
console.log("When we try to access the private member outside the class : " + _myceillingFan);
//Ans: When we try to access the private member outside the class : The fan Brand is : undefined


