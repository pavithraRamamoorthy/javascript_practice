/* 
5. Consider below array: var alphabets = ['A', 'E', 'C', 'D', 'Z', 'X', 'T', 'U'] Sort them in ascending order.
*/

var alphabets = ['A', 'E', 'C', 'D', 'Z', 'X', 'T', 'U']

console.log("The ascending order of alphabets : " + alphabets.sort());
//Ans: The ascending order of alphabets : A,C,D,E,T,U,X,Z
