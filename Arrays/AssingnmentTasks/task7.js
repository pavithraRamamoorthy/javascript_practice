/* 
7. Fix the logic to handle money in strings. 
Goal: To find total money available in the "penHolder" (including int in string) 
*/

var penHolder = ["refil", "cap", "spacePen", "pencil", "clip", "eraser", "nib", "metalHolder", 
"markers", "sketches", "10", 20, "100", 0.5, "A", "B", "C", "D", "stapler", 200, 200.5, "a", "b", "c"] 

var totalMoneyInPenholder = 0; 

for(var i=0; i< penHolder.length; i++)
{ 
        var convention = parseFloat(penHolder[i]);
        if(! isNaN(convention))
        { 
                totalMoneyInPenholder += convention; 
        } 
} 
console.log(totalMoneyInPenholder); 