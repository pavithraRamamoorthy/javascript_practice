/*
3. You can mutate an the value at an index in an array by using the square brackets. 
Does the same thing work with a string? Why might that be? 
*/ 
//Example1:

var mylist = []
mylist = ['hello']

mylist
// ["hello"]

mylist[1] = "world!";

console.log(mylist);
// (2) ["hello", "world!"]

//Example2:

var mylists = [10, 20, 30, 40, 50]
//(5) [10, 20, 30, 40, 50]

mylists[0] = 100;
//100

console.log(mylists)
//(5) [100, 20, 30, 40, 50]