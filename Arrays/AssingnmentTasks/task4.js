/* 
4. Create two arrays of vowels, integers (0 to 9). Print the combination of these two arrays
*/

var vowels = ["a", "e", "i", "o", "u"]
var integers = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]

//var combos = []

//console.log("The combination of two arrays is : " + combos.valueOf());


console.log( "concat the two arrays is : " + vowels.concat(integers));
//Ans: (15) ["a", "e", "i", "o", "u", 0, 1, 2, 3, 4, 5, 6, 7, 8, 9]

console.log( "join the two arrays : " + vowels.join(integers));
//Ans: a0,1,2,3,4,5,6,7,8,9e0,1,2,3,4,5,6,7,8,9i0,1,2,3,4,5,6,7,8,9o0,1,2,3,4,5,6,7,8,9u

//Example2

combineArr = [] //or combos = new Array(2);

for(var i = 0; i < vowels.length; i++)
{
     for(var j = 0; j < integers.length; j++)
     {
        //you would access the element of the array as array1[i] and array2[j]
        //create and array with as many elements as the number of arrays you are to combine
        //add them in
        combineArr.push(vowels[i] + integers[j])
     }
} 
console.log("combination of two arrays is : " + combineArr);
/*(50) ["a0", "a1", "a2", "a3", "a4", "a5", "a6", "a7", "a8", "a9", 
"e0", "e1", "e2", "e3", "e4", "e5", "e6", "e7", "e8", "e9", 
"i0", "i1", "i2", "i3", "i4", "i5", "i6", "i7", "i8", "i9", 
"o0", "o1", "o2", "o3", "o4", "o5", "o6", "o7", "o8", "o9", 
"u0", "u1", "u2", "u3", "u4", "u5", "u6", "u7", "u8", "u9"] */

