/*
1. What happens when you access an element outside the length of the string with charAt?
*/
//Ans:

var person = "PavithraRamamoorthy";

console.log("The person of value length is : " + person.length);
//Ans: 19

console.log(person.charAt(19));
//Ans: ""

//person.charAt(length-1);

console.log(person.charAt(18));
//Ans: "y"

/*
2. What happens when you do the same thing with the square brackets?
*/
//Ans:

var person = "PavithraRamamoorthy";

console.log("The person of value length is : " + person.length);
//Ans: 19

console.log(person[18]);
//Ans: "y"

console.log(person[19]);
//Ans: undefined

