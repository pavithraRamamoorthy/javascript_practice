
// Goal: Add two numbers and return a value
    /* i/p: 
    10, 20.60
    20, "50"
    "20", 50
    20, 50
    "C", "D" => we should reject & send them an error message
	"2CD", "CD" => we should reject & send them an error message
	"2CD", "34CD" => o/p=36
    */
function addNumbers(operandOne, operandTwo){
        var sumOfInputs  =  0;
        var errorMessage =  {"message" : "The inputs should be valid numbers or convertible strings. Please fix & try again."}
        /*
            1. get the inputs
            2. validate the inputs
            3. implement the business logic
            4. return the output
        */ 

        // inputs are operandOne, operandTwo
        var validationResponse = validateInputs(operandOne, operandTwo);
        if(!validationResponse){
            return errorMessage;
        }
        // Business logic
        sumOfInputs = parseFloat(operandOne) + parseFloat(operandTwo);
        return sumOfInputs;
}

function validateInputs(operandOne, operandTwo){
        // small & has one responsibility

        var validationResult = true;
        // validation logic
        /* validation: 
            - the inputs should be numbers (make sure that the inputs are not strings). 
            - The strings that are convertible into numbers should be accepted
        */
        // Fix the logic
        if( isNaN(parseFloat(operandOne)) || isNaN(parseFloat(operandTwo))){
            return false;
        }
        return validationResult;
}

console.log(addNumbers("20.90", 50.80));//71.69999999999999
console.log(addNumbers(10, 20.60)); //30.6
console.log(addNumbers(20, "50")); //70
console.log(addNumbers("20", 50)); //70
console.log(addNumbers(20, 50)); //70
console.log(addNumbers("C", "D")); //{ message: 'The inputs should be valid numbers or convertible strings. Please fix & try again.'}
console.log(addNumbers("2CD", "CD"));// { message: 'The inputs should be valid numbers or convertible strings. Please fix & try again.'}
console.log(addNumbers("2CD", "34CD"));// 36
