// a Book class using ES6 class syntax
class Book {
    constructor(title, author) {
        this.title = title;
        this.author = author;
    }

    describeBook() {
        var description = this.title + " by " + this.author + ".";
        return description;
    }
}

function favoriteBook()
{
        return { title: "The Guards", author: "Ken Bruen" };
}
   
module.exports.favoriteBook = favoriteBook;
module.exports.Book = Book;
