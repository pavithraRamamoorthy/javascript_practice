// returns the favorite book
function favoriteBook() {
    return { title: "The Guards", author: "Ken Bruen" };
}
 
// returns a list of books
function getBook() {
    return [
        {id: 1, title: "The Guards", author: "Ken Bruen"},
        {id: 2, title: "The Stand", author: "Steven King"},
    ];
}
 
// exports the variables and functions above so that other modules can use them
module.exports.favoriteBook = favoriteBook;
module.exports.getBook = getBook;