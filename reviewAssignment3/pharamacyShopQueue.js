/*  pseudocode:
Goal: Optimize the queue for better service in your shop.

1. Input is online order type and cost for medicine.
2. create a class called queueOfOnlineOrders.
3. Create a method for onlineOrders and pass the two inputs for orderType, costOfMedicine
4. validate the inputs.
5. Three types of online order requests is there.
6. check the condition, if the request for orderType is life critical means the request is processed in 15 minitues,
and the profit is calculated based on 20% of the total cost.

7. check the condition, if the request for orderType is one day delay is allowed means the request is processed in 30 minitues,
and the profit is calculated based on 10% of the total cost.

8. check the condition, if the request for orderType is cannot wait for delay more than 30 hours means the request is processed in 55 minitues,
and the profit is calculated based on 5% of the total cost.

9. Created a new object for class and use that object and call the method for onlineOrders and pass the inputs.
10. finally get the maximum profit and print the output for console.

*/




class queueOfOnlineOrders
{
        onlineOrders(orderType, costOfMedicine)
        {
                var maxProfit = {};
                var profit = 0;
                if(orderType === "life critical")
                {
                        console.log("procees your request in 15 min");
                        profit += (costOfMedicine/100)*20;
                        maxProfit["maximumProfit"] = "life critical profit : " + profit;
                        //console.log(profit);
                }

                else if(orderType === "one day delay is allowed")
                {
                        console.log("procees your request in one day");
                        profit += (costOfMedicine/100)*10;
                        maxProfit["maximumProfit"]= "one day delay is allowed profit : " + profit;
                        //console.log(profit);
                }

                else if(orderType === "cannot wait for delay more than 30 hours")
                {
                        console.log("procees your request in 30 hours");
                        profit += (costOfMedicine/100)*5;
                        maxProfit["maximumProfit"] = "cannot wait for delay more than 30 hours profit : " + profit;
                        //console.log(profit);
                }
                return maxProfit;

        }
}    
var _pharmacyShop =  new queueOfOnlineOrders();   
console.log(_pharmacyShop.onlineOrders("life critical", 5000));
console.log(_pharmacyShop.onlineOrders("one day delay is allowed", 5000));
console.log(_pharmacyShop.onlineOrders("cannot wait for delay more than 30 hours", 5000));
