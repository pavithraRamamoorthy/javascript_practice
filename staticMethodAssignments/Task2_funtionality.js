//2. Can you extend a functionality of a function? How you will do, write an example.

//Example1
/*
Rabbit extends Animal creates two [[Prototype]]

1. Rabbit function prototypally inherits from Animal function.
2. Rabbit.prototype prototypally inherits from Animal.prototype.
*/
class Animal {}
class Rabbit extends Animal {}

// for statics
console.log(Rabbit.__proto__ === Animal); // true

// for regular methods
console.log(Rabbit.prototype.__proto__ === Animal.prototype); // true


//Example 2
function Employee(firstName,lastName)  
{  
  this.firstName=firstName;  
  this.lastName=lastName;  
}  
  
Employee.prototype.fullName=function()  
  {  
    return this.firstName+" "+this.lastName;  
  }  
  
var employee1=new Employee("Pavithra","Ramamoorthy");  
var employee2=new Employee("Chenchu", "Suresh");  
console.log(employee1.fullName());  
console.log(employee2.fullName());  
/*Ans: Pavithra Ramamoorthy
Chenchu Suresh
*/