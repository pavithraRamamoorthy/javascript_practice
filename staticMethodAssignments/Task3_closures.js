
//3. Explain closure. Write an example using a cell phone.

/* closure is a self Executing function.
Closure means that an inner function always has access to the variables and parameters of its outer function, 
even after the outer function has returned.
*/

//Example1

var  cellPhone = [
  {"name" : "Nokia", "cost" : 5000},
  {"name" : "Apple", "cost" : 20000},
  {"name" : "Samsung", "cost" : 13000},
  {"name" : "oppo", "cost" : 15000},
  {"name" : "Lenovo", "cost" : 10000}
];
var cellPhoneName = cellPhone.filter(value => {
  if (value.cost == 20000)
  {
    return value.name;
  }
}).map(element => element.name);

console.log(cellPhoneName);//Ans:["Apple"]

//Example2

var  cellPhone = [
        {"name" : "Nokia", "cost" : "5000"},
        {"name" : "Apple", "cost" : "20,000"},
        {"name" : "Samsung", "cost" : "13,000"},
        {"name" : "oppo", "cost" : "15,000"},
        {"name" : "Lenovo", "cost" : "10,000"}
];
cellPhone.forEach((element, index, array) => {
        console.log(element.cost);
        console.log(index); 
        console.log(array); 
});
/*Ans: 5000
0
[
  { name: 'Nokia', cost: '5000' },
  { name: 'Apple', cost: '20,000' },
  { name: 'Samsung', cost: '13,000' },
  { name: 'oppo', cost: '15,000' },
  { name: 'Lenovo', cost: '10,000' }
]
20,000
1
[
  { name: 'Nokia', cost: '5000' },
  { name: 'Apple', cost: '20,000' },
  { name: 'Samsung', cost: '13,000' },
  { name: 'oppo', cost: '15,000' },
  { name: 'Lenovo', cost: '10,000' }
]
13,000
2
[
  { name: 'Nokia', cost: '5000' },
  { name: 'Apple', cost: '20,000' },
  { name: 'Samsung', cost: '13,000' },
  { name: 'oppo', cost: '15,000' },
  { name: 'Lenovo', cost: '10,000' }
]
15,000
3
[
  { name: 'Nokia', cost: '5000' },
  { name: 'Apple', cost: '20,000' },
  { name: 'Samsung', cost: '13,000' },
  { name: 'oppo', cost: '15,000' },
  { name: 'Lenovo', cost: '10,000' }
]
10,000
4
[
  { name: 'Nokia', cost: '5000' },
  { name: 'Apple', cost: '20,000' },
  { name: 'Samsung', cost: '13,000' },
  { name: 'oppo', cost: '15,000' },
  { name: 'Lenovo', cost: '10,000' }
]
*/
      