//1. How you will access static method from a child class (the static method is in a parent class).

//Example 1

class Base {
        static greet() {
           return this.Name; 
        }
      } 
     
class Child extends Base {  } 
     
console.log("The child class access the parent class static method, it returns the name for : " + (Child.Name = "Indra")); 
//Ans: The child class access the parent class static method, it returns the name for : Indra 

//Example2

class Parent {
        static parentName() {
                return this.parentName = "Ramamoorthy";
        }
    
        constructor() {
            this.parentAge = 50;
            this.parentJob = "Agriculture";
        }
    
        calculateParentAge() {
                if (this.parentAge >= 45)
                {
                        console.log("My parent age is : " + this.parentAge + "and he is  doing an : " + this.parentJob);
                } 
        }
    }    
console.log(Parent.parentName());
var _parentClass = new Parent();
console.log(_parentClass);

class Child extends Parent {
        constructor() {  
                super();  
                this.childName = "Pavithra";  
                this.childAge = 23;  
        }
        calculateParentAge() {
                if (this.parentAge >= 45)
                {
                        return "The age is : " + this.parentAge + "," +  "and he is  doing an : " + this.parentJob;
                } 
        }

}
    
var _child = new Child(); 
console.log(_child.childName + "is a JavaScript Developer and the age is " + _child.childAge );
console.log("My Father Name is " + Child.parentName + "," + _child.calculateParentAge());
/* Ans: Pavithrais a JavaScript Developer and the age is 23
My Father Name is Ramamoorthy,The age is : 50,and he is  doing an : Agriculture
*/