class Square
{
        constructor(width = 8, height = 8)
        {
                this.width = width;
                this.height = height;
        }

        calculateSquare()
        {
                return this.width * this.height;
        }

        static validateSquareValues(width, height)
        {
                return width === height;
        }
}

var _square = new Square();
console.log(_square.calculateSquare()); //64

console.log(Square.validateSquareValues(10, 10)); //true