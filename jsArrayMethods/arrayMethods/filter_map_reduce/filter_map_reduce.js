var students = [
        { "stuId" : 1, "stuName" : "Pavithra", "subjectA" : 80, "subjectB" : 85, "MCA" : true},
        { "stuId" : 2, "stuName" : "Indra", "subjectA" : 90, "subjectB" : 95, "MCA" : false },
        { "stuId" : 3, "stuName" : "Nandhu", "subjectA" : 80, "subjectB" : 90, "MCA" : true },
]

//Filter()
var studentData = students.filter(function (student) {
        return student.MCA;
});
console.log(studentData)
/*Ans:[
  {
    stuId: 1,
    stuName: 'Pavithra',
    subjectA: 80,
    subjectB: 85,
    MCA: true
  },
  {
    stuId: 3,
    stuName: 'Nandhu',
    subjectA: 80,
    subjectB: 90,
    MCA: true
  }
]

*/

//Map()
var studentScores = studentData.map(function (studentsubjects) {
        return stusubjects.subjectA + studentsubjects.subjectB;
});
console.log(studentScores)
//Ans: [ 165, 170 ]

//Reduce()
var totalStudentScore = studentScores.reduce(function (a,b) {
        return a + b;
}, 0);
console.log(totalStudentScore)
//Ans: 335