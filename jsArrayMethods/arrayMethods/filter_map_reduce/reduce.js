/* using forEach()
var numbers = [1, 2, 3, 4, 5],
total = 0;
     
numbers.forEach(function (number) {
    total += number;
});
console.log(total)
Ans: 15
*/

//Example1 

var total = [1, 2, 3, 4, 5].reduce(function (previous, current) {
        return previous + current;
}, 0);
console.log("count the total numbers in an array using reduce is : " + total)
//Ans: count the total numbers in an array using reduce is : 15

//Example2

var total = [1, 2, 3, 4, 5].reduce(function (previous, current, index) {
        var val = previous + current;
        console.log("The previous value is " + previous + 
                    "; the current value is " + current +
                    ", and the current iteration is " + (index + 1));
        return val;
}, 0);
     
console.log("The final value is " + total + ".");
/* Ans:The previous value is 0; the current value is 1, and the current iteration is 1
The previous value is 1; the current value is 2, and the current iteration is 2
The previous value is 3; the current value is 3, and the current iteration is 3
The previous value is 6; the current value is 4, and the current iteration is 4
The previous value is 10; the current value is 5, and the current iteration is 5
The final value is 15.
*/