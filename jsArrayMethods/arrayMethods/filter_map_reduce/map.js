//Example 1
var employees = [{ "id" : 1, "name" : "PavithraRamamoorthy" },
        { "id" : 2, "name" : "Indra" },
        { "id" : 3, "name" : "ChenchuSuresh" },
        { "id" : 4, "name" : "ChenchuRam" },
        { "id" : 5, "name" : "Nandhu"}
      ];

var employeeIds = employees.map(function (emp) {
        return emp.id
});

console.log("The employeeIds are : " + employeeIds)
//Ans: The employeeIds are : 1,2,3,4,5

/* using ES6:

var employeeIds = employees.map(emp => emp.id);
Ans: [ 1, 2, 3, 4, 5 ]
*/

//Example2

var tasks = [{ "name" : "Object assignment", "duration" : 120 },
        { "name" : "Array methods", "duration" : 60 },
        { "name" : "String Functions", "duration" : 180 },
        { "name" : "JavaScript Conditions", "duration" : 240 },
        ];
var task_names = tasks.map(function (task, index, array) {
        return task.name;     
});
console.log("Get the task_names using map : " + task_names)
//Ans: Get the task_names using map : Object assignment,Array methods,String Functions,JavaScript Conditions


/* using ES6
var task_names = tasks.map((task) => task.name );
console.log(task_names)
Ans: [
        'Object assignment',
        'Array methods',
        'String Functions',
        'JavaScript Conditions'
      ]
*/