//Example 1

var tasks = [{ "name" : "Object assignment", "duration" : 120 },
        { "name" : "Array methods", "duration" : 60 },
        { "name" : "String Functions", "duration" : 180 },
        { "name" : "JavaScript Conditions", "duration" : 240 },
        ];

var duration_tasks = tasks.filter(function (task) {
        return task.duration >= 120;
});
console.log(duration_tasks)
/*Ans: [
  { name: 'Object assignment', duration: 120 },
  { name: 'String Functions', duration: 180 },
  { name: 'JavaScript Conditions', duration: 240 }
]
*/
             
/* Using ES6
var duration_tasks = tasks.filter((task) => task.duration >= 120 );        
console.log(duration_tasks)
Ans: [
  { name: 'Object assignment', duration: 120 },
  { name: 'String Functions', duration: 180 },
  { name: 'JavaScript Conditions', duration: 240 }
]
*/

//Example2

var employees = [{ "id" : 1, "name" : "PavithraRamamoorthy" },
        { "id" : 2, "name" : "Indra" },
        { "id" : 3, "name" : "ChenchuSuresh" },
        { "id" : 4, "name" : "ChenchuRam" },
        { "id" : 5, "name" : "Nandhu"}
      ];
var empName = employees.filter(function (emp) {
        return emp.name === "PavithraRamamoorthy";
});
console.log(empName)
//Ans: [ { id: 1, name: 'PavithraRamamoorthy' } ]
      
var empName1 = employees.filter(function (emp) {
        return emp.name === "Indra";
});
console.log(empName1)
//Ans: [ { id: 2, name: 'Indra' } ]

/*Using ES6
     
var empName = employees.filter(emp => emp.name === "PavithraRamamoorthy");
var empName1 = employees.filter(emp => emp.name === "Indra");
console.log(empName, empName1)
Ans: [ { id: 1, name: 'PavithraRamamoorthy' } ] [ { id: 2, name: 'Indra' } ]
*/