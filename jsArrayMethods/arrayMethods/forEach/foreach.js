//Example 1

var employees = [{ "id" : 1, "name" : "PavithraRamamoorthy" },
        { "id" : 2, "name" : "Indra" },
        { "id" : 3, "name" : "ChenchuSuresh" },
        { "id" : 4, "name" : "ChenchuRam" },
        { "id" : 5, "name" : "Nandhu"}
      ];

var employeeIds = [];
employees.forEach(function (emp) {
        employeeIds.push(emp.id);
});      
console.log("The employeeIds are : " + employeeIds)
//Ans: The employeeIds are : 1,2,3,4,5

//Example 2

// Durations are in minutes
 
var tasks = [{ "name" : "Object assignment", "duration" : 120 },
        { "name" : "Array methods", "duration" : 60 },
        { "name" : "String Functions", "duration" : 180 },
        { "name" : "JavaScript Conditions", "duration" : 240 },
        ];
var task_names = [];
 
tasks.forEach(function (task) {
         
        task_names.push(task.name);
             
});     
console.log(task_names)   
/*Ans: [
  'Object assignment',
  'Array methods',
  'String Functions',
  'JavaScript Conditions'
]
*/
//Example 3

var duration_tasks = [];
 
tasks.forEach(function (task) {
    if (task.duration >= 120) {
        duration_tasks.push(task);
    }
});
console.log(duration_tasks)
/* Ans:[
  { name: 'Object assignment', duration: 120 },
  { name: 'String Functions', duration: 180 },
  { name: 'JavaScript Conditions', duration: 240 }
]
*/ 