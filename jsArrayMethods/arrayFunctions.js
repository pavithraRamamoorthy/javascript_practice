//Create an Array

var characters = ["pavithra", "chenchuRam", "chenchu","suresh"];
console.log("Length of the array is : " + characters.length)
// Length of the array is : 4

//Access an Array item using the index position

var first = characters[0]
console.log("The first character in an array is : " + first);
// The first character in an array is : pavithra

var last = characters[characters.length - 1]
console.log("The last character in an array is : " + last);
// The last character in an array is : suresh

// Loop over an Array

characters.forEach(function(item, index, array) {
  console.log(item, index)
})
// pavithra 0
// chenchuRam 1
//chenchu 2
//suresh 3

//Add an item to the end of an Array

var total = characters.push("Indra");
 
console.log(characters); // ["pavithra", "chenchuRam", "chenchu","suresh","Indra"]
console.log(total);      // 5 

//Remove an item from the end of an Array

var last = characters.pop() 
console.log("Remove an item from the end of an Array : " + last)
// Remove an item from the end of an Array : Indra

//Remove an item from the beginning of an Array

var first = characters.shift() 
console.log("Remove an item from the beginning of an Array : " + first);
// Remove an item from the beginning of an Array : pavithra

//Add an item to the beginning of an Array

var newLength = characters.unshift("Priya") // add to the front
console.log("Add an item to the beginning of an Array : " + newLength);


//Find the index of an item in the Array

console.log("##########" + characters.push('Bhavani'));
// 5

var pos = characters.indexOf("Bhavani");
console.log("Find the index of an item in the Array : " + pos);
// 4

//Remove an item by index position

let removedItem = characters.splice(pos, 1);
console.log("Remove an item by index position is : " + removedItem);                                        
// Bhavani

//toString():

var allcharacters = characters.toString();
console.log(allcharacters)
//priya,chenchuRam,chenchu,suresh


