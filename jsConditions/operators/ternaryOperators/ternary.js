//Ternary Operator:
        //Ternary Operator is a special operator :? that assigns a value to a variable based on some condition. 
        //This is like short form of if-else condition.
        //Ternary operator ?: is a conditional operator.

//Syntax:
        //<condition> ? <value1> : <value2>;        

/* Example1 */

var a = 10;
var b = 5;

var c = a > b? a : b; 

var d = a > b? b : a;     

console.log("The value of c is : " + c); // value of c would be 10
console.log("The value of d is : " + d); // value of d would be 5        

/* Example2 */

var speed = 90;
var car = speed >= 120 ? 'Too Fast' : (speed >= 80 ? 'Fast' : 'OK');
console.log("The car speed is : " + car);

/* Example3 */

/*var age = 19;
var canDrive;
if (age > 16) {
    console.log(canDrive = 'yes');
} else {
    console,log(canDrive = 'no');
}
*/

var age = 19;
var canDrive = age > 16 ? 'yes' : 'no';
console.log(canDrive);
