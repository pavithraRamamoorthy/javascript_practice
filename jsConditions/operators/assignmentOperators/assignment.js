//Assignment operators to assign values to variables.

var x = 5, y = 10, z = 15;

console.log(x = y); //x would be 10

console.log(x += 1); //x would be 6

console.log(x -= 1); //x would be 4

console.log(x *= 5); //x would be 25

console.log(x /= 5); //x would be 1

console.log(x %= 2); //x would be 1
