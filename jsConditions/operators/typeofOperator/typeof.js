//typeof is a keyword that will return the type of a variable when you call it.

var a = 10;
var b = "pavi";
var c = true;


var x = "pavithra";
var y = x.concat("Ramamoorthy");
console.log(y);
var z = {"name" : "pavi", "qualification" : "MCA" }

var fruits = ["apple", "mango", "orange"]

console.log("Type of a is : " + typeof a);
console.log("Type of b is : " + typeof b);
console.log("Type of true is : " + typeof c);
console.log("Type of y is" + typeof y);
console.log("Type of z is : " + typeof z);
console.log("Type of fruits is : " + typeof fruits);
