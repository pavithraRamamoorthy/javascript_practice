//Arithmetic operators are used to perform mathematical operations between numeric operands.

var x = 5;
var y = 10;
var z = x+y;

var a = y-x;
var b = x*y;
var c = y/x;
var d = x%2;
var e = x++;
var f = x--;
var g = y++;
var h = y--;
console.log("The Addition value is :" + z);
console.log("The Subtraction value is :" + a);
console.log("The Multiplication value is :" + b);
console.log("The Division value is :" + c);
console.log("The Modulus value is :" + d);
console.log("The postIncreament of x  value is :" + e);
console.log("The postDecreament of x value is :" + f);
console.log("The postIncreament of y value is :" + g);
console.log("The postDecreament of y value is :" + h);