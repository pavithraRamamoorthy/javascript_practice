//Comparison Operators:
//compare two operands and return Boolean value true or false.


var a = 5, b = 10, c = "5";
var x = a;

console.log("a Is equal to x is : " + (a == c)); // returns true

console.log("Identical (equal and of same type) is :" + (a === c)); // returns false

console.log("a is equal to x is  :  " + (a == x)); // returns true

console.log("a Not equal to b is : " + (a != b)); // returns true

console.log("a Greater than b value is : " + (a > b)); // returns false

console.log("a Less than b value is : " + (a < b)); // returns true

console.log("a Greater than or equal to b is : " + (a >= b)); // returns false

console.log("a Less than or equal to b is : " + (a <= b)); // returns true

console.log("a Greater than or equal to c is  : " + (a >= c)); // returns true

console.log("a Less than or equal to c is : " + (a <= c)); // returns true