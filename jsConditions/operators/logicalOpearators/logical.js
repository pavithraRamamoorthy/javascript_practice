
//Logical operators are used to combine two or more conditions.

var a = 5, b = 10;

console.log((a != b) && (a < b)); // returns true

console.log((a > b) || (a == b)); // returns false

console.log((a < b) || (a == b)); // returns true

console.log(!(a < b)); // returns false

console.log(!(a > b)); // returns true