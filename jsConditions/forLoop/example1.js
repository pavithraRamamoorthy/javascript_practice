/*for statement:
        A for loop repeats until a specified condition evaluates to false.

Syntax:
        for ([initialExpression]; [condition]; [incrementExpression])
                statement 
*/

/* Example1 */

var str = "";

for (let i = 0; i < 9; i++) {
  str = str + i;
}

console.log(str);

/* Example2 */

var count;

console.log("Starting Loop")

for(count = 0; count < 10; count++) 
{
        console.log("Current Count : " + count );
}         

console.log("Loop stopped!");

/* Example3 */

for (i=1; i<=5; i++)  
{  
console.log(i)  
}  
