//Switch statement:
	
	//Use this statement if you want to select one of many blocks of code to execute.

/* Eample1 */

var date = new Date()
var theDay = date.getDay()
switch (theDay)
{
case 0:
console.log("Sunday")
break
        
case 1:
console.log("Monday")
break

case 2:
console.log("Tuesday")
break

case 3:
console.log("Wednesday")
break

case 4:
console.log("Thrusday")
break

case 5:
console.log("Finally Friday")
break

case 6:
console.log("Saturday")
break

default:
console.log("Does not match theDay")
}