
//if....else statement:

    //If you want to execute some code if a condition is true and another code if a condition is false.

/* Eample1 - Ans:My Salary is greater than your salary */

var mySal = 1000;
var yourSal = 500;

if( mySal > yourSal)
{
	console.Log("My Salary is greater than your salary");
}

else
{
	console.log("My Salary is less than or equal to your salary");
}

