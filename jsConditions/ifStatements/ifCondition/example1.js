//if statement: Use if conditional statement if you want to execute some code based on some condition. 

//Eample1 - Ans: My Salary is greater than your salary 

var mySal = 1000;
var yourSal = 500;

if( mySal > yourSal)
{
	console.log("My Salary is greater than your salary");
}