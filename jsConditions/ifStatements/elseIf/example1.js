//else if statement:

	//Use "else if" condition when you want to apply second level condition after if statement.

/* Eample1 - Ans:My Salary is greater than your salary */

var mySal = 1000;
var yourSal = 500;

if( mySal > yourSal)
{
			
	console.log("My Salary is greater than your salary");
}
		
else if(mySal < yourSal)
{
			
	console.log("My Salary is less than your salary");
}
		
else if(mySal == yourSal)
{
			console.log("My Salary is equal to your salary");
		}

else {
	
	console.log(mySal);
}		