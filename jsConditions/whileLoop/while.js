/*
while statement:
        A while statement executes its statements as long as a specified condition evaluates to true.

Syntax:
        while (condition)
          statement
*/  

/* Example1  sum of n numbers */

var n = 0;
var x = 0;
while (n < 3) {
  x += n;
  n++;
}
console.log("The value of x is " + x)

/* Example2 */

var i = 1;  
while (i <= 10)  
{  
console.log(i);  
i++;  
}  