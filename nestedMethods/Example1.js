function count(i) { 
  var count = i;
  var displayCount = function() {
      console.log(count++);
      count++;
  };
  return displayCount; 
}

var myCount = count(1);
myCount(); // 1
myCount(); // 2
myCount(); // 3

// new closure - new outer scope - new contor variable
var myOtherCount = count(10);
myOtherCount(); // 10 
myOtherCount(); // 11