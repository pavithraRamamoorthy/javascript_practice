function counter() {
   var count = 0;
   return function () {
     return count++;
   };
 }
 
 var countValue1 = counter();
 var countValue2 = counter();
console.log(countValue1());  // 0
console.log(countValue1());  // 1
console.log(countValue2());  // 0
console.log(countValue2());  // 1
console.log(countValue2());  // 2