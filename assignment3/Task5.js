/* 3. Write a function that converts an array of array to an array of strings without duplicates.

input: [ [10, 20],  ["10",20], ["A20", 20] , ["ABCD20", 20] ]
output (order does not matter):  ["10", "A20", "20" , "ABCD20"]

*/

//Example1: Using forEach()

var arr = [ [10, 20],  ["10",20], ["A20", 20] , ["ABCD20", 20] ]
var singleArray  = arr.flat(); //flat() method creates a new array with all sub-array elements
//var singleArray  = [].concat(...arr); //merge multidimensional array into a single array.
console.log("The array of array converted into single array : " + singleArray);  
        
        
var finalArray = singleArray.map(String);
console.log(finalArray)
console.log("The array converted into string : " + finalArray);

function removeDuplicates(array)
{
        var emptyArray = []
        array.forEach((value) => {
                if (emptyArray.indexOf(value) == -1)
                {
                        emptyArray.push(value);
                      
                }
        });

        return emptyArray;
}
console.log("The final array without duplicates are : " + removeDuplicates(finalArray));
/* Ans: The array of array converted into single array : 10,20,10,20,A20,20,ABCD20,20
(8) ["10", "20", "10", "20", "A20", "20", "ABCD20", "20"]
The array converted into string : 10,20,10,20,A20,20,ABCD20,20
The final array without duplicates are : 10,20,A20,ABCD20
*/


// //Example2: using includes()


var arr = [ [10, 20],  ["10",20], ["A20", 20] , ["ABCD20", 20] ]
var singleArray  = arr.flat(); //flat() method creates a new array with all sub-array elements
//var singleArray  = [].concat(...arr); //merge multidimensional array into a single array.
console.log("The array of array converted into single array : " + singleArray);  
        
        
var finalArray = singleArray.map(String);
console.log(finalArray)
console.log("The array converted into string : " + finalArray);
function removeDuplicate(array)
{
        var emptyArray = []
        array.forEach((value) => {
                if (!emptyArray.includes(value))
                {
                        emptyArray.push(value);
                      
                }
        });

        return emptyArray;
}
console.log("The final array without duplicates are : " + removeDuplicate(finalArray));