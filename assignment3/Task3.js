/*
3. Write a function that calculates and returns the area square of an array of  rectangles. 
Formula: Area Square = length * width.
Input:
rectangles = [ [10, 20],  ["10",20], ["A20", 20] , ["ABCD20", 20] ]

Expected output: [ 200, 200,  400, 400]
*/

var rectangles = [ [10, 20],  ["10",20], ["A20", 20] , ["ABCD20", 20] ]
var emptyArray = []
function getAreaSquareRectangle(rectangles)
{
        for (var i = 0; i< rectangles.length; i++)
        {
                var result = rectangles[i];
  
                var rectLength = Number(String(result[result.length-2]).replace(/\D/g,''));
                var rectWidth = Number(String(result[result.length-1]).replace(/\D/g,''));
                
               
                if (!isNaN(rectLength) && !isNaN(rectWidth))
                {
                        emptyArray.push(rectLength * rectWidth);       
                }
        }
        return emptyArray
}
console.log(getAreaSquareRectangle(rectangles)); 
//Ans:  [ 200, 200, 400, 400 ]


