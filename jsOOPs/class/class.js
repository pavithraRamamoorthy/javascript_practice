//Declaring class  
class Employee  
{  
//Initializing an object  
    constructor(id,name)  
    {  
      this.id=id;  
      this.name=name;  
    }  
//Declaring method  
    detail()  
    {  
        console.log(this.id+" "+this.name)  
    }  
}  
//passing object to a variable   
var _employee1=new Employee(1,"Pavithra");  
var _employee2=new Employee(2,"Indra");  
_employee1.detail(); //calling method  
_employee2.detail();  
/*Ans:1 Pavithra
2 Indra
*/