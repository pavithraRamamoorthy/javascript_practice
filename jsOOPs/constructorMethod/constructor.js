//Example of a constructor method.
//The constructor keyword is used to declare a constructor method.It is used to initialize and create an object.
class Employee {  
        constructor() {  
                this.id=101;  
                this.name = "Indra";  
        }   
}  
var _employee = new Employee();  
console.log(_employee.id+" "+_employee.name); 
//Ans: 101 Indra

//Example2
class CompanyName  
{  
  constructor()  
  {  
    this.company="TCS";  
  }  
}  
class Employees extends CompanyName {  
  constructor(id,name) {  
   super();  //The super keyword is used to call the parent class constructor
    this.id=id;  
    this.name=name;  
  }   
}     
var _employee = new Employees(1,"Indra");  
console.log(_employee.id+" "+_employee.name+" "+_employee.company);  
//Ans: 1 Indra TCS