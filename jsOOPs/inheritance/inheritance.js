/*Inheritance:
        - The extends keyword is used in class expressions or class declarations.
        - Using extends keyword, we can acquire all the properties and behavior of the inbuilt object 
        as well as custom classes.
*/

// we declare sub-class that extends the properties of its parent class.

class Bike  
{  
  constructor()  
  {  
    this.company="Honda";  
  }  
}  
class Vehicle extends Bike {  
  constructor(name,price) {  
    super();  
    this.name=name;  
    this.price=price;  
  }   
}  
var _vehicle = new Vehicle("bike","70000");  
console.log(_vehicle.company+" "+_vehicle.name+" "+_vehicle.price);  
//Ans: Honda bike 70000