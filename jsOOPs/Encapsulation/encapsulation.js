/*Encapsulation: Encapsulation is a process of binding the data.
        - Use var keyword to make data members private.
        - Use setter methods to set the data and getter methods to get that data.
*/

class Student  
{  
        constructor()  
        {  
        var name;  
        var marks;  
        }  
        
        getName()  
        {  
                return this.name;  
        }  
        
        setName(name)  
        {  
                this.name=name;  
        }  
        
        getMarks()  
        {
                return this.marks;  
        }  
        setMarks(marks)  
        {  
                this.marks=marks;  
        }  
  
}  
var _student=new Student();  
_student.setName("PavithraRamamoorthy");  
_student.setMarks(80);  
console.log(_student.getName()+" "+_student.getMarks())
//Ans: PavithraRamamoorthy 80