/*Realtime object : House

House Plan
    - design / architecture
        - basic idea of the house
            - rooms
            - windows
            - doors
            - roofs (1)
            - walls
            - pillars (40)
            - cost (estimate)
*/            

class house{
    // private
   saleAgreement= {
        "owner": "RajuR",
        "ready": true
    };
	//var #COSTPERSQMETER = 2200;

    constructor(rooms=20, windows=12, doors=6, roofs=1, walls=24, pillars=30, baseCost=10000)
    { // default behaviour
        this.rooms      = 6; // 'rooms' belong to 'house'
        // Data variable
        // Data member
        // 'rooms' is visible to other objects inherited from this one
        this.windows    = windows;
        this.doors      = doors;
        this.roofs      = roofs;
        this.walls      = walls;
        this.pillars    = pillars;
        this.baseCost   = baseCost;
    }
    calculateEstimate(){
    // Method
    // Function member

        this.cost = this.rooms * this.roofs * this.baseCost;
        return this.cost;
    }
}            

/*Blue print
=> class 
=> name is: house
*/

var myHouse = new house(); // Default values
var _cost   = myHouse.calculateEstimate()
    console.log(_cost.windows);//Ans:  undefined

var myBrothersHouse = new house(24, 12, 3, 24, 60, 100000);
var _costOfBrothersHouse = myBrothersHouse.calculateEstimate()
    console.log(_costOfBrothersHouse);//Ans: 1440000 

var numberOfFloors  = myHouse.roofs;
console.log(numberOfFloors);//Ans: 1
// Reading properties/attributes from an object
var numberOfPillars = myHouse.pillars;
console.log(numberOfPillars);//Ans: 30


var myHouse = new house(3,12);
console.log(myHouse);//Ans: [object Object]
/*=> 5L
=> Object creation
*/

var _building = new house(6);
console.log(_building)
/*=> 50L
=> different object
*/

var _warehouse = new house(1, 200);
console.log(_warehouse)
/*=> 50 Crores
=> different object
*/


/* Inheritance */
/*
way of speaking
behaviour/activities
height
...
*/
class townHouse extends house{
/*
Parent (Super class): 'house'
Child: 'townHouse'
So, Child will inherit all data members, methods from parent.
*/

}

var _townHouse = new townHouse();
console.log(_townHouse.rooms); //Ans: 6 

    _townHouse.calculateEstimate();
    // 60000 
    // calling super class's method
    
    console.log("@@@@@" + _townHouse.saleAgreement) // Ans: @@@@@[object Object]
    // we are trying to call a private member. Does this work in child object?

class bungalow extends house{
/*
child: 'bungalow'
parent: 'house'
*/
    // Overrides calculateEstimate() from 'house'
    // Method overriding
    calculateEstimate(){
        this.cost = 10 * this.rooms * this.roofs * this.baseCost;
        return this.cost;
    }
}

var _bungalow = new bungalow();
console.log(_bungalow.calculateEstimate()); //Ans: 600000
// calls calculateEstimate() from 'bungalow'
