//Example1
//way to perform a single action in different forms.
//A child class object invokes the parent class method.
 
class A  
  {  
     display()  
    {  
      console.log("Hello World!");  
    }  
  }  
class B extends A  
  {  
          display()
          {
                console.log("Welcome to JavaScript");
          }
        
  }  
var b=new B();  
console.log(b.display());
//Ans: Welcome to JavaScript

//Example2

class Parent  
  {  
     display()  
    {  
      console.log("Hello World!");  
    }  
  }  
class Child extends Parent  
  {  
    display()  
    {  
      console.log("Welcome to India!");  
    }  
  }  
  
var a = new Parent();
console.log(a.display());
//Ans: Hello World!
var b = new Child();
console.log(b.display());
//Ans: Welcome to India!